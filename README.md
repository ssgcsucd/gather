## gather Basics

2020 Ken Gribble University of California, Davis — Computer Science department

Running this linux bourne shell script will gather informaion
and logs about the system it is run on. It also gives the user
a little time to control-c out before it runs. It gives instructions
as it runs.

Information will be in a tar gzipped archive file named 
after the hostname of the system similar to this structure,
(commands run):

    ├── cpu
    │   └── proc-cpuinfo (cat /proc/cpuinfo)
    ├── drives
    │   └── df-h (df -h)
    ├── gather-run.log (the log produced by the gather program)
    ├── memory
    │   └── proc-meminfo (cat /proc/meminfo)
    ├── network
    │   ├── ifconfig (ifconfig -a)
    │   ├── netstat (netstat)
    │   └── ping.google.com (pings google.com)
    ├── processes
    │   ├── ps-ef (ps -ef)
    │   └── w (w)
    └── systemlogs
        ├── dmesg (dmesg)
        ├── last.0 (last --hostlast)
        ├── last.1 (last --hostlast --file /var/log/wtmp.1)
        └── logs.tar.gz (a tar gzipped archive of /var/log)

## TODO
* feature add: more pings
* feature add: my ip as seen from outside network (if wget then wget -O - https://ipv4.ipleak.net/json/ | grep ip)
* clean up documentation
* improve UI — put in anything that might help users
* add OS detection if needed
